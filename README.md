# Ty Topico

TyTopico paketi; istenilen - desteklenen web kaynaklarindan günün ilgili top iceriklerini getirmeyi sağlar.

Bu paket ile tüm projelerde kolayca dahil edilp kullanilabilecek, ayrıca geliştirilebilir yapisiyla yeni alicilar kolaylikla
eklenebilmesine imkan sağlar. Açık kaynak dunyasina katki sağlamak amaciyla sunulmustur.

Bu proje desteklerle bir cok alanda yeni icerik kaynagi desteginin saglanmasi ile daha da buyumek ve yeni gelistirmelere alt yapi saglayacaktir.

## Feature
- Gelistirilebilir yapi
- Bir cok icerik saglayici kaynagi
- Yeni icerik kaynaklariyla gelismeye acik
- Composer paket destegi, kolay kurulum, entegre
- Testler
- PSR-4
- Design Patterns

## Todos
 - Logger
 - New Receives

## Requires & Dependency
- PHP Version >= 5.5
- Guzzle Php Http Client
- Symfony Dom Crawler
- Twitter Php Library use abraham/twitteroauth

## Version
- v1.0.0

## Installation
Download bitbucket or composer
- Bitbucket: https://bitbucket.org/tahsinyuksel/tytopico
- Composer Install: composer require tahsinyuksel/ty-topico

Bitbucket


```sh
$ git clone https://tahsinyuksel@bitbucket.org/tahsinyuksel/tytopico.git
```


Composer

```sh
$ composer require tahsinyuksel/ty-topico
```

## Examples
- Tests: Test folder in test and example actions

## License
MIT

## Contact
Questions, suggestions, comments:


Tahsin Yüksel
info@tahsinyuksel.com