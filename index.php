<?php

require_once __DIR__ . '/vendor/autoload.php';

use Ty\TyTopico\Hello;
use Ty\TyTopico\Tests\ContentTest;

echo "index page";

// Runs all the files under the folder (src)
echo Hello::world();

// test page run
echo ContentTest::getHealth();