<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/10/16
 * Time: 15:31
 */

namespace Ty\TyTopico\Service\Receives\Trends;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class GoogleTrendsReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'googletrends';
        $this->siteType = 'trends';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://www.google.com.tr/';
        $this->sourcePrefix     = 'trends/hottrends/atom/feed?pn=';
        $this->sourceType       = 'p24';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $crawler = new Crawler();
        $crawler->addXmlContent(utf8_decode($sourceStr));
        //$feedItem = $crawler->filter('channel > item');
        $feedItem = $crawler->filterXPath('//channel/item');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> trim($feedItem->eq($i)->filterXPath('//title')->text()),
                    "link"=> 'https://www.google.com/search?q=' . trim($feedItem->eq($i)->filterXPath('//title')->text()),
                    "thumbImage" => ltrim($feedItem->eq($i)->filterXPath('//ht:picture')->text(), '//'),
                    "date" => date('Y-m-d H:i:s', strtotime( $feedItem->eq($i)->filterXPath('//pubDate')->text()))
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}