<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/10/16
 * Time: 14:52
 */

namespace Ty\TyTopico\Service\Receives\Trends;

use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class GoogleTrendsTitlesReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'googletrends';
        $this->siteType = 'trends';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://www.google.com/trends/';
        $this->sourcePrefix     = 'hottrends/visualize/internal/';
        $this->sourceType       = 'data';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $data = json_decode($sourceStr, true);
        if(isset($data['turkey']) && !empty($data['turkey'])) {

            $feedItem = $data['turkey'];
            for($i = 0; $i < count($feedItem); $i++){

                $item = array(
                    "id" => $this->createId($i + 1),
                    'name'=> $feedItem[$i],
                    'link'=> 'https://www.google.com/search?q=' . $feedItem[$i]
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}