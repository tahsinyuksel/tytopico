<?php

/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/10/16
 * Time: 18:29
 */
namespace Ty\TyTopico\Service\Receives\Video;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

/**
/**
 * youtube rss read
 *
 * example:
 * https://www.youtube.com/feeds/videos.xml?channel_id=CHANNELID channel
 * https://www.youtube.com/feeds/videos.xml?user=USERNAME user result
 * https://www.youtube.com/feeds/videos.xml?playlist_id=YOUR_YOUTUBE_PLAYLIST_NUMBER playlist data
 *
 * @link http://stackoverflow.com/questions/29752447/how-to-get-a-youtube-channel-rss-feed-after-2015-april-20-without-v3-api
 *
 * Class YoutubeReceive
 * @package Ty\TyTopico\Service\Receives\Video
 */
class YoutubeReceive extends BaseReceiveAbstract
{
    // Playlist id: turkey trends
    private $playlistId = 'PLOITePbU6f2YZBIc6Oh6r35zRLpoiPS-6';

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'youtube';
        $this->siteType = 'video';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://www.youtube.com/feeds/videos.xml';
        $this->sourcePrefix     = '?playlist_id=';
        $this->sourceType       = $this->playlistId;

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array());

        if($sourceStr == '') {
            return $result;
        }

        $doc = new \DOMDocument();
        $doc->loadXML($sourceStr);

        $result = array();
        $i = 1;
        foreach ($doc->getElementsByTagName('entry') as $feeditem)
        {
            $media = $feeditem->getElementsByTagName('group')->item(0) ;
            $published = new \DateTime($feeditem->getElementsByTagName('published')->item(0)->nodeValue);

            $item = array(
                "id" => $this->createId($i + 1),
                "title"=> $feeditem->getElementsByTagName('title')->item(0)->nodeValue,
                "link" => $media->getElementsByTagName('content')->item(0)->getAttribute('url'),
                "thumbImage" => $media->getElementsByTagName('thumbnail')->item(0)->getAttribute('url'),
                "date"=> $published->format('Y-m-d H:i:s')
            );

            $result[] = array_merge($metaData, $item);

            if($i == $top){
                break;
            }
            $i += 1;
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }
}