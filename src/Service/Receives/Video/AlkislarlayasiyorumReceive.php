<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/10/16
 * Time: 14:12
 */

namespace Ty\TyTopico\Service\Receives\Video;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class AlkislarlayasiyorumReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'alkislarlayasiyorum';
        $this->siteType = 'video';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'http://alkislarlayasiyorum.com/';
        $this->sourcePrefix     = 'popon';
        $this->sourceType       = '';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $html = '<html>' . $sourceStr . '</html>';

        $crawler = new Crawler($html);
        $feedItem = $crawler->filter('a.popon-content-a');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $img = $feedItem->eq($i)->filter('img');

                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> trim($feedItem->eq($i)->attr('title')),
                    "thumbImage" => $img->attr('src'),
                    "link" => $feedItem->eq($i)->attr('href'),
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}