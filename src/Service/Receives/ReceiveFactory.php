<?php

/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/09/16
 * Time: 21:46
 */
namespace Ty\TyTopico\Service\Receives;

use Ty\TyTopico\Service\Receives\Dictionary\EksisozlukReceive;
use Ty\TyTopico\Service\Receives\Dictionary\InstelaReceive;
use Ty\TyTopico\Service\Receives\Dictionary\UludagsozlukReceive;
use Ty\TyTopico\Service\Receives\News\GooglenewsReceive;
use Ty\TyTopico\Service\Receives\Social\OnedioReceive;
use Ty\TyTopico\Service\Receives\Trends\GoogleTrendsReceive;
use Ty\TyTopico\Service\Receives\Trends\GoogleTrendsTitlesReceive;
use Ty\TyTopico\Service\Receives\Twitter\TwitterReceive;
use Ty\TyTopico\Service\Receives\Video\AlkislarlayasiyorumReceive;
use Ty\TyTopico\Service\Receives\Video\YoutubeReceive;

/**
 * Class ReceiveFactory
 * @package Ty\TyTopico\Service\Receives
 */
class ReceiveFactory
{
    private static $instanceObjects = array();

    public static function create($type = '', $singleton = true)
    {
        $objName = strtolower($type);

        /** --------------------- Singleton pattern --------------------- */
        if($singleton == true) {

            if(isset(self::$instanceObjects[$objName]) && self::$instanceObjects[$objName] != null) {
                return self::$instanceObjects[$objName];
            }
        }

        /** --------------------- create factory --------------------- */
        $instance = null;
        switch ($objName)
        {
            case 'eksisozluk':
                $instance = new EksisozlukReceive();
                break;

            case 'instela':
                $instance = new InstelaReceive();
                break;

            case 'uludagsozluk':
                $instance = new UludagsozlukReceive();
                break;

            case 'twitter':
                $instance = new TwitterReceive();
                break;

            case 'youtube':
                $instance = new YoutubeReceive();
                break;
            
            case 'alkislarlayasiyorum':
                $instance = new AlkislarlayasiyorumReceive();
                break;

            case 'googlenews':
                $instance = new GooglenewsReceive();
                break;

            case 'googletrendstitles':
                $instance = new GoogleTrendsTitlesReceive();
                break;

            case 'googletrends':
                $instance = new GoogleTrendsReceive();
                break;

            case 'onedio':
                $instance = new OnedioReceive();
                break;
        }

        /** --------------------- Set Singleton object --------------------- */
        if($singleton == true && $instance != null) {
            self::$instanceObjects[$objName] = $instance;
        }

        return $instance;
    }
}