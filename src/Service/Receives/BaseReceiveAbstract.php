<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 28.09.2016
 * Time: 22:39
 */

namespace Ty\TyTopico\Service\Receives;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * Dependency Guzzle
 *
 * Class BaseReceiveAbstract
 * @package Ty\TyTopico\Service\Receives
 */
abstract class BaseReceiveAbstract implements ReceiveTopicInterface
{
    /** @var string must set value in receive object */
    protected $source = '';

    /** @var string must set value in receive object */
    protected $siteType = '';

    /** @var string  */
    protected $category = '';

    /** @var string  */
    protected $type = 'popular';

    /** ------------------------- source operations ------------------------- **/
    /** @var string  */
    protected $sourceRouteUrl = '';

    /** @var string  */
    protected $sourcePrefix = '';

    /** @var string  */
    protected $sourceType = '';

    /** @var null  */
    protected $httpClient = null;

    /** @var LoggerInterface|null  */
    protected $logger = null;

    /**
     * Dependency Guzzle
     *
     * BaseReceiveAbstract constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param LoggerInterface|null $logger
     */
    public function setLogger(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $val
     * @return int
     */
    public function createId($val = '')
    {
        $timestamp = strtotime(date('Y-m-d'));
        $result = (string)$timestamp . (string)$val;
        return (integer)$result;
        //return crc32($val);
    }

    /**
     * @param array $data
     */
    public function setOptionData($data = array())
    {
        foreach($data as $key=> $val) {
            $this->$key = $val;
        }
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getSiteType()
    {
        return $this->siteType;
    }

    /**
     * @param string $category
     */
    public function setCategory($category = '')
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $type
     */
    public function setType($type = 'popular')
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get point result string
     * TODO: factory method usable. sendRequest[curl, guzzle, etc.]
     * @param $method
     * @param string $uri
     * @param array $options
     * @return string
     */
    protected function getSourcePoint($method, $uri = '', array $options = [])
    {
        return $this->guzzleRequest($method, $uri, $options);
    }

    protected function guzzleRequest($method, $uri = '', array $options = [])
    {
        $this->httpClient = new Client();

        $response = $this->httpClient->request($method, $uri, $options);
        if($response->getStatusCode() >= 200 && $response->getStatusCode() <= 202) {
            return utf8_encode($response->getBody()->getContents());
        }

        return '';
    }

    /**
     * Use Curl request
     * @param $method
     * @param string $uri
     * @param array $options
     * @return mixed
     */
    protected function curlRequest($method, $uri = '', array $options = [])
    {
        $method = strtolower($method);

        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $uri);

        if(isset($options['headers']) && !empty($options['headers']))
        {
            $headers = array();
            foreach ($options['headers'] as $headerKey => $headerVal) {
                $headers[] = $headerKey . ':' . $headerVal;
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if($method != 'get') {

            if(isset($options['form_params']) && !empty($options['form_params']))
            {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $options['form_params']);
            }
        }

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        // $output contains the output string
        $output = curl_exec($ch);
        //$output = utf8_decode(curl_exec($ch));

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    /**
     * @return array
     */
    public function getTopicMeta()
    {
        return array(
            "source"=> $this->source,
            "type"=> $this->type,
            "date"=> date('Y-m-d H:i:s')
        );
    }
}