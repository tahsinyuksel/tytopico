<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/09/16
 * Time: 16:45
 */
namespace Ty\TyTopico\Service\Receives;

use Ty\TyTopico\Service\Receives\ReceiveTopicInterface;

interface ReceiveManagerInterface extends ReceiveTopicInterface
{
    public function createReceive($receive, $singleton = true);

    public function getReceive($receive);

    public function setReceive($receive);

    public function hasReceive($receive);

    public function getActiveReceives();
}