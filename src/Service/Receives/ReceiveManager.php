<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/09/16
 * Time: 16:44
 */

namespace Ty\TyTopico\Service\Receives;

use Ty\TyTopico\Service\Receives\ReceiveManagerInterface;
use Ty\TyTopico\Service\Receives\ReceiveTopicInterface;
use \Psr\Log\LoggerInterface;

/**
 * Receive operations
 * Warning: receiveList file is parameters. This should load.
 *
 * Class ReceiveManager
 * @package Ty\TyTopico\Service\Receives
 */
class ReceiveManager implements ReceiveManagerInterface
{
    /** @var ReceiveTopicInterface|null  */
    public $receive = null;

    public $receiveList = array();

    public $logger = null;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;

        try{
            $this->receiveList = include (__DIR__ . '/receiveList.php');

        } catch (\Exception $e) {
            exit('receiveList.php file not found');
        }
    }

    /**
     * @param $receive
     * @param bool $singleton
     * @return mixed|null|\Ty\TyTopico\Service\Receives\ReceiveTopicInterface
     */
    public function createReceive($receive, $singleton = true)
    {
        return ReceiveFactory::create($receive, $singleton);
    }

    /**
     * @param $receive
     * @return null|\Ty\TyTopico\Service\Receives\ReceiveTopicInterface
     */
    public function getReceive($receive)
    {
        return $this->receive;
    }

    /**
     * @param $receive
     * @return bool
     */
    public function setReceive($receive)
    {
        if($this->hasReceive($receive)) {

            $receiveObject = $this->createReceive($receive);
            if($receiveObject != null) {
                $this->receive = $receiveObject;

                // Set receive logger
                $this->receive->setLogger($this->logger);
                return true;
            }
        }

        return false;
    }

    /**
     * @param $receive
     * @return bool
     */
    public function hasReceive($receive)
    {
        $receive = strtolower($receive);
        foreach ($this->getActiveReceives() as $activeReceive) {
            if(isset($activeReceive[$receive])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getActiveReceives()
    {
        return $this->receiveList;
    }

    public function getTop($top = 5)
    {
        return $this->receive->getTop($top);
    }

    public function getTopByCategory($category = '', $top = 5)
    {
        return $this->receive->getTopByCategory($category, $top);
    }

    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        return $this->receive->getTopByCategoryInType($category, $type, $top);
    }

}