<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 16.12.2015
 * Time: 20:25
 */

namespace Ty\TyTopico\Service\Receives\News;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class GooglenewsReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'googlenews';
        $this->siteType = 'news';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'http://news.google.com.tr/';
        $this->sourcePrefix     = 'news';
        $this->sourceType       = '?cf=all&hl=tr&pz=1&ned=tr_tr&output=rss';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $html = simplexml_load_string($sourceStr);

        if($html == null){
            return $result;
        }

        $i = 0;

        foreach ($html->channel->item as $item)
        {
            preg_match('@src="([^"]+)"@', $item->description, $match);
            //$parts = explode('<font size="-1">', $item->description);

            $parts = parse_url((string) $item->link);
            parse_str($parts['query'], $query);
            $link = $query['url'];

            $item = array(
                "id" => $this->createId($i + 1),
                "title"=> trim((string) $item->title),
                "link" => (string) $link,
                "thumbImage" => $match[1],
            );

            $result[] = array_merge($metaData, $item);

            if($i + 1 == $top){
                break;
            }

            $i++;
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}