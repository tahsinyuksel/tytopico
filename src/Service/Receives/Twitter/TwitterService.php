<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 15.12.2015
 * Time: 15:34
 */

namespace Ty\TyTopico\Service\Receives\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterService
{
    const CONSUMER_KEY = '0nmPZXx3mMgB1570qLuWv1d7a';
    const CONSUMER_SECRET = 'ZMHcvyjCdaFuQLo508no4vka1aDpBsxk8AiYUsGZPEo4anED4I';
    const ACCESS_TOKEN = '4397881461-wRLp1x4ZzOJKkxhIV00I2zBU54II3P8VXlmS5ff';
    const ACCESS_TOKEN_SECRET = 'Fm8WO0EicV6nMwcHS9v70UkZBJtDJq5kws6NZB7G9813D';

    /**
     * @var TwitterOAuth
     */
    public $connection;

    public function __construct()
    {
        $this->connection = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET, self::ACCESS_TOKEN, self::ACCESS_TOKEN_SECRET);
        //API Version
        //$this->connection->host = 'https://api.twitter.com/1.1/';
        //$content = $this->connection->get("account/verify_credentials");
        //print_r($content);
    }

    public function getTrendTopics($top = 10, $woeId = '23424969')
    {
        // $woeId default value is Turkey
        if($woeId == ''){
            $woeId = '23424969';
        }

        $result = array();

        $ret = $this->connection->get('/trends/place', array('id' => $woeId));
        if($ret && isset($ret[0]->trends))
        {
            for($i = 0; $i < $top; $i++){
                $topic = $ret[0]->trends[$i];
                $result[] = array(
                    'name'=> $topic->name,
                    'link'=> $topic->url
                );
            }
        }

        return $result;
    }
}
