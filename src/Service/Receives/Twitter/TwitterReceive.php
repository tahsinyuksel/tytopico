<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/10/16
 * Time: 17:38
 */

namespace Ty\TyTopico\Service\Receives\Twitter;


use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;
use Ty\TyTopico\Service\Receives\Twitter\TwitterService;

/**
 * Dependency
 * TwitterService: twitter api used
 *
 * Class TwitterReceive
 * @package Ty\TyTopico\Service\Receives\Twitter
 */
class TwitterReceive extends BaseReceiveAbstract {

    private $twitterService = null;

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'twitter';
        $this->siteType = 'twitter';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://twitter.com/';
        $this->sourcePrefix     = 'i/';
        $this->sourceType       = 'trends?k=&pc=true&show_context=false&src=search-home';

        /** ------------------------- dependency ------------------------- **/
        $this->twitterService = new TwitterService();

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        if($result = $this->twitterService->getTrendTopics($top)){
            return $result;
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}