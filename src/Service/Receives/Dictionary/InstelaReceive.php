<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 30/09/16
 * Time: 22:54
 */

namespace Ty\TyTopico\Service\Receives\Dictionary;


use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class InstelaReceive extends BaseReceiveAbstract
{
    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'instela';
        $this->siteType = 'dictionary';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://tr.instela.com/';
        $this->sourcePrefix     = 'list/';
        $this->sourceType       = 'mostpopular/1?ajaxload=true';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $options = array(
            'Accept-Charset'=> 'UTF-8',
            'Accept-Language'=> 'tr',
            'Content-type'=> 'text/html; charset=UTF-8'
        );

        $sourceStr = $this->getSourcePoint('GET', $url, $options);

        if($sourceStr == '') {
            return $result;
        }

        $crawler = new Crawler();
        $crawler->addHtmlContent(utf8_decode($sourceStr));
        $feedItem = $crawler->filter('ul.mode-mostpopular > li > a');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> trim($feedItem->eq($i)->attr('title')),
                    "link" => $this->sourceRouteUrl . $feedItem->eq($i)->attr('href')
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }
}