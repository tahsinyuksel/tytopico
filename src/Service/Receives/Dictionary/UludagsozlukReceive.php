<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/10/16
 * Time: 16:52
 */

namespace Ty\TyTopico\Service\Receives\Dictionary;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class UludagsozlukReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'uludagsozluk';
        $this->siteType = 'dictionary';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'http://uludagsozluk.com/';
        $this->sourcePrefix     = 'gundem/';
        $this->sourceType       = '';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        // TODO: issue: guzzle empty body content
        $sourceStr = $this->curlRequest('GET', $url);

        if($sourceStr == '') {
            return $result;
        }

        $crawler = new Crawler($sourceStr);
        $feedItem = $crawler->filter('ul.index-list > li > a');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> trim($feedItem->eq($i)->text()),
                    "link" => $this->sourceRouteUrl . $feedItem->eq($i)->attr('href'),
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }

}