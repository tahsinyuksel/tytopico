<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 28.09.2016
 * Time: 22:54
 */
namespace Ty\TyTopico\Service\Receives\Dictionary;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class EksisozlukReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'eksisozluk';
        $this->siteType = 'dictionary';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'https://eksisozluk.com/';
        $this->sourcePrefix     = 'basliklar/';
        $this->sourceType       = 'populer';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $html = '<html>' . $sourceStr . '</html>';

        $crawler = new Crawler(utf8_decode($html));
        $feedItem = $crawler->filter('ul.topic-list > li > a');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> trim($feedItem->eq($i)->text()),
                    "link" => $this->sourceRouteUrl . $feedItem->eq($i)->attr('href'),
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}