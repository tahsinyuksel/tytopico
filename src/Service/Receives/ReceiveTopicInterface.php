<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 28.09.2016
 * Time: 22:36
 */
namespace Ty\TyTopico\Service\Receives;

interface ReceiveTopicInterface
{
    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5);

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5);

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5);
}