<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 16.12.2015
 * Time: 23:17
 */

namespace Ty\TyTopico\Service\Receives\Social;

use Symfony\Component\DomCrawler\Crawler;
use Ty\TyTopico\Service\Receives\BaseReceiveAbstract;

class OnedioReceive extends BaseReceiveAbstract {

    /**
     * Receive must set define
     */
    public function __construct()
    {
        /** ------------------------- must define set receive identity info ------------------------- **/
        $this->source   = 'onedio';
        $this->siteType = 'social';
        $this->category = '';

        /** ------------------------- source operations ------------------------- **/
        $this->sourceRouteUrl   = 'http://onedio.com/';
        $this->sourcePrefix     = 'ara/haber/';
        $this->sourceType       = '?sort=popularity%20desc&date=day';

        parent::__construct();
    }

    /**
     * @param int $top
     * @return array
     */
    public function getTop($top = 5)
    {
        $metaData = $this->getTopicMeta();
        $result = array();

        $url = $this->sourceRouteUrl . $this->sourcePrefix . $this->sourceType;
        $sourceStr = $this->getSourcePoint('GET', $url, array('X-Requested-With'=> 'XMLHttpRequest'));

        if($sourceStr == '') {
            return $result;
        }

        $crawler = new Crawler(utf8_decode($sourceStr));
        $feedItem = $crawler->filter('article header > a > img');

        if($feedItem->count() > 0)
        {
            for($i = 0; $i < $feedItem->count(); $i++)
            {
                $img = $feedItem->eq($i);
                $a = $feedItem->eq($i)->parents()->filter('a');

                $item = array(
                    "id" => $this->createId($i + 1),
                    "title"=> (trim($a->attr('title'))),
                    "link" => $this->sourceRouteUrl . $a->attr('href'),
                    "thumbImage" => $img->attr('src'),
                );

                $result[] = array_merge($metaData, $item);

                if($i + 1 == $top){
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $category
     * @param int $top
     * @return array
     */
    public function getTopByCategory($category = '', $top = 5)
    {
        // TODO: Implement getTopByCategory() method.
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $top
     * @return array
     */
    public function getTopByCategoryInType($category = '', $type = '', $top = 5)
    {
        // TODO: Implement getTopByCategoryInType() method.
    }


}