<?php

/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 01/10/16
 * Time: 14:44
 */
namespace Ty\TyTopico\Lib\Logger;

use Psr\Log\LoggerInterface;

class TyLogger implements LoggerInterface
{
    /** @var null|LoggerInterface  */
    public $logger = null;

    /**
     * Logger constructor.
     */
    public function __construct(LoggerInterface $logger = null)
    {
        if($logger != null) {
            $this->logger = $logger;
        }
    }

    private function runLog($name, $message, $context = array())
    {
        if($this->logger != null) {
            try{
                $this->logger->$name($message, $context);
            }catch (\Exception $e) {
                throw new \Exception('Log call method error or not found. ' . $e->getMessage(), $e->getCode());
            }
        }
    }

    public function emergency($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function alert($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function critical($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function error($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function warning($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function notice($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function info($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function debug($message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }

    public function log($level, $message, array $context = array())
    {
        $this->runLog(__FUNCTION__, $message, $context);
    }
}